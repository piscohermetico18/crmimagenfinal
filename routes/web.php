<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Route::get('/users', 'Auth\UserController@index')->name('users.index');

Route::get('/user/create', 'Auth\UserController@create')->name('user.create');
Route::post('/user/store', 'Auth\UserController@store')->name('user.store');
Route::get('/user/{id}/edit', 'Auth\UserController@edit')->name('user.edit');
Route::put('/user/update', 'Auth\UserController@update')->name('user.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('inicio');
Route::get('/instituciones', 'InstitucionController@index')->name('instituciones');

Route::get('/tipos_evento', 'TipoEventoController@index')->name('tipos_evento');
Route::get('/tipos_evento/create', 'TipoEventoController@create')->name('tipos_evento.create');
Route::post('/tipos_evento', 'TipoEventoController@store')->name('tipos_evento.store');
Route::get('/tipos_evento/{id}/edit', 'TipoEventoController@edit')->name('tipos_evento.edit');
Route::put('/tipos_evento/update', 'TipoEventoController@update')->name('tipos_evento.update');
Route::get('/tipos_evento/{id}/delete', 'TipoEventoController@destroy')->name('tipos_evento.delete');

Route::get('/eventos', 'EventoController@index')->name('eventos');
Route::get('/eventos/create', 'EventoController@create')->name('eventos.create');
Route::post('/eventos', 'EventoController@store')->name('eventos.store');
Route::get('/eventos/{id}/edit', 'EventoController@edit')->name('eventos.edit');
Route::put('/eventos/update', 'EventoController@update')->name('eventos.update');
Route::get('/eventos/{id}/delete', 'EventoController@destroy')->name('eventos.delete');
Route::get('/eventos/{id}/detalle', 'EventoController@show')->name('eventos.show');

Route::get('/eventos/searchajax', 'EventoController@autoComplete')->name('eventos.searchajax');

Route::get('/personas_evento/{id_evento}/{id_persona}/detalle', 'PersonaEventoController@show')->name('personas_evento.show');
Route::get('/personas_evento/{id_evento}/{id_institucion}/create', 'PersonaEventoController@create')->name('personas_evento.create');
Route::post('/personas_evento', 'PersonaEventoController@store')->name('personas_evento.store');
Route::get('/personas_evento/{id}/edit', 'PersonaEventoController@edit')->name('personas_evento.edit');
Route::put('/personas_evento/update', 'PersonaEventoController@update')->name('personas_evento.update');
Route::get('/personas_evento/{id}/delete', 'PersonaEventoController@destroy')->name('personas_evento.delete');

Route::get('/personas_evento/{tipo_doc}/{num_doc}/getByDoc', 'PersonaEventoController@getByDoc')->name('personas_evento.getByDoc');



Route::get('/instituciones', 'InstitucionController@index')->name('instituciones');
Route::post('/instituciones/datatable', 'InstitucionController@datatableInstitucion')->name('instituciones.datatable');
Route::get('/institucion/create', 'InstitucionController@create')->name('institucion.create');
Route::post('/institucion/store', 'InstitucionController@store')->name('institucion.store');
Route::get('/institucion/{id}/edit', 'InstitucionController@edit')->name('institucion.edit');
Route::put('/institucion/update', 'InstitucionController@update')->name('institucion.update');

Route::post('/institucion/storecontacto', 'InstitucionController@storeContacto')->name('institucion.storecontacto');

Route::post('/contactos/datatable', 'contactoController@datatableContactos')->name('contactos.datatable');
Route::get('/contacto/create/{institucion_id}', 'contactoController@create')->name('contacto.create');
Route::post('/contacto/store', 'contactoController@store')->name('contacto.store');
Route::get('/contacto/{id}/edit', 'contactoController@edit')->name('contacto.edit');
Route::put('/contacto/update', 'contactoController@update')->name('contacto.update');




Route::post('/componentes/select/provincia', 'Componentes\UbigeoController@selectProvincia')->name('provincia_select');
Route::post('/componentes/select/distrito', 'Componentes\UbigeoController@selectDistrito')->name('distrito_select');

