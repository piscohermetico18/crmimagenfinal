@extends('layouts.app')
@section('title')
<h3>
Registrar persona
  </h3>
@endsection
@section('content')
  @if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <i class=" fa fa-check cool-green "></i>
        {{ nl2br(Session::get('success')) }}
    </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-block alert-warning">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('warning')) }}
      </div>
  @endif
  @if(Session::has('danger'))
      <div class="alert alert-block alert-danger">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('danger')) }}
      </div>
  @endif
<div class="portlet-body">

                <div class="panel-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  {!! Form::model($persona_evento, [
                    'method' => 'PUT',
                    'url' => ['personas_evento/update'],
                    'class' => 'tab-content',
                    'id'=>'formulario'
                    ]) !!}
                        {{ csrf_field() }}
                        {!! Form::hidden('id', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'id'  ]) !!}
                        {!! Form::hidden('id_institucion', $persona_evento->persona->id_institucion, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_institucion'  ]) !!}
                        {!! Form::hidden('id_persona', $persona_evento->id_persona, ['class' => 'form-control', 'id' => 'id_persona'  ]) !!}
                        {!! Form::hidden('id_evento', $persona_evento->id_evento, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_evento'  ]) !!}
                        <div class="row">
                          <div class="col-md-4">
                              <label>Tipo de documento:</label>
                              <div class="input-group col-md-12 form-group {{ $errors->has('tipo_doc') ? 'has-error' :'' }}">
                                  {!! Form::select('tipo_doc', $datosTiposDoc, $persona_evento->persona->tipo_doc, ['class' => 'form-control', 'required'=>'required', 'id' => 'tipo_doc']) !!}
                                  {!! $errors->first('tipo_doc','<span class="help-block">:message</span>') !!}
                              </div>
                          </div>
                            <div class="col-md-4">
                                <label>Número de documento:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('num_doc', $persona_evento->persona->num_doc, ['class' => 'form-control', 'required'=>'required', 'id' => 'num_doc' , 'placeholder'=>'Ingrese numero de documento'  ]) !!}
                                </div>
                            </div>
                          </div>
                          <hr/>
                          <div class="other">
                          <div class="row">
                            <div class="col-md-6">
                                <label>Apellido paterno:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('apellido_paterno', $persona_evento->persona->apellido_paterno, ['class' => 'form-control', 'required'=>'required', 'id' => 'apellido_paterno' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Apellido materno:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('apellido_materno', $persona_evento->persona->apellido_materno, ['class' => 'form-control', 'id' => 'apellido_materno' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Primer nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('primer_nombre', $persona_evento->persona->primer_nombre, ['class' => 'form-control', 'required'=>'required', 'id' => 'primer_nombre' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Segundo nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('segundo_nombre', $persona_evento->persona->segundo_nombre, ['class' => 'form-control', 'id' => 'segundo_nombre' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Fecha de nacimiento</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_nacimiento', $persona_evento->persona->fecha_nacimiento->format('Y-m-d'), ['class' => 'form-control form-control datepicker', 'required'=>'required', 'id' => 'fecha_nacimiento' , 'placeholder'=>''  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Género:</label>
                                <div class="input-group col-md-12 form-group {{ $errors->has('genero') ? 'has-error' :'' }}">
                                    {!! Form::select('genero', $datosGeneros, $persona_evento->persona->genero, ['class' => 'form-control', 'required'=>'required', 'id' => 'genero']) !!}
                                    {!! $errors->first('genero','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Estado Civil:</label>
                                <div class="input-group col-md-12 form-group {{ $errors->has('estado_civil') ? 'has-error' :'' }}">
                                    {!! Form::select('estado_civil', $datosEstadosCivil, $persona_evento->persona->estado_civil, ['class' => 'form-control', 'required'=>'required', 'id' => 'estado_civil']) !!}
                                    {!! $errors->first('estado_civil','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                          <div class="col-md-4">
                              <label>Telefóno celular:</label>
                              <div class="input-group col-md-12 form-group">
                                  {!! Form::text('telefono_celular', $persona_evento->persona->telefono_celular, ['class' => 'form-control', 'required'=>'required', 'id' => 'telefono_celular' , 'placeholder'=>''  ]) !!}
                              </div>
                          </div>
                            <div class="col-md-4">
                                <label>Telefóno fijo:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('telefono_fijo', $persona_evento->persona->telefono_fijo, ['class' => 'form-control', 'id' => 'telefono_fijo' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                              <div class="col-md-4">
                                  <label>Correo electrónico:</label>
                                  <div class="input-group col-md-12 form-group">
                                      {!! Form::email('email', $persona_evento->persona->email, ['class' => 'form-control', 'id' => 'email' , 'placeholder'=>'', 'required'=>'required'  ]) !!}
                                  </div>
                              </div>
                        </div>
                        <hr/>

                        <div class="row">
                          <div class="col-md-4">
                              <label>Año de término de la educación secundaria:</label>
                              <div class="input-group col-md-12 form-group">
                                  {!! Form::text('anio_termino', $persona_evento->persona->anio_termino, ['class' => 'form-control form-control dateYearpicker', 'required'=>'required', 'id' => 'anio_termino' , 'placeholder'=>'' ]) !!}
                                  <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <hr/>
                        <div class="row">
                          <div class="col-md-4">
                              <label>Estado Actual:</label>
                              <div class="input-group col-md-12 form-group {{ $errors->has('id_estado') ? 'has-error' :'' }}">
                                  {!! Form::select('id_estado', $datosEstados, $persona_evento->persona->id_estado, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_estado']) !!}
                                  {!! $errors->first('id_estado','<span class="help-block">:message</span>') !!}
                              </div>
                          </div>
                            <div class="col-md-4">
                                <label>Comentario:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::textarea('comentario', $persona_evento->comentario, ['class' => 'form-control', 'id' => 'comentario' , 'placeholder'=>'', 'rows' => '2'  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-check">
                                    {!! Form::checkbox('confirmado', 1, $persona_evento->confirmado, ['class' => 'form-check-input', 'id' => 'confirmado'  ]) !!}
                                    <label class="form-check-label" for="confirmado">Confirmado</label>
                                </div>
                                <div class="form-check">
                                    {!! Form::checkbox('asistencia', 1, $persona_evento->asistencia, ['class' => 'form-check-input', 'id' => 'asistencia'  ]) !!}
                                    <label class="form-check-label" for="asistencia">Asistencia</label>
                                </div>
                            </div>
                          </div>
                          <hr/>
                        <div class="row">
                            @include('componentes.submit_reset_form_update')
                        </div>
                      </div>

                    </div>
                    {!! Form::close() !!}

                </div>
</div>

@endsection

@section('js')
  <script>
      $('.datepicker').datepicker({autoclose: true, format: 'yyyy-mm-dd',});
      $('.dateYearpicker').datepicker({
        format: 'yyyy',
        dateFormat: 'yy',
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
      });
  </script>
@endsection
