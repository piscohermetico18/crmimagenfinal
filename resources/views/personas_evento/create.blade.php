@extends('layouts.app')
@section('title')
<h3>
Registrar persona
  </h3>
@endsection
@section('content')
  @if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <i class=" fa fa-check cool-green "></i>
        {{ nl2br(Session::get('success')) }}
    </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-block alert-warning">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('warning')) }}
      </div>
  @endif
  @if(Session::has('danger'))
      <div class="alert alert-block alert-danger">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('danger')) }}
      </div>
  @endif
<div class="portlet-body">

                <div class="panel-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                    <form method="POST" action="{{ url('personas_evento') }}">
                        {{ csrf_field() }}
                        {!! Form::hidden('id_estado', 1, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_estado'  ]) !!}
                        {!! Form::hidden('id_institucion', $id_institucion, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_institucion'  ]) !!}
                        {!! Form::hidden('id_persona', null, ['class' => 'form-control', 'id' => 'id_persona'  ]) !!}

                        {!! Form::hidden('id_evento', $id_evento, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_evento'  ]) !!}
                        <div class="row">
                          <div class="col-md-4">
                              <label>Tipo de documento:</label>
                              <div class="input-group col-md-12 form-group {{ $errors->has('tipo_doc') ? 'has-error' :'' }}">
                                  {!! Form::select('tipo_doc', $datosTiposDoc, null, ['class' => 'form-control', 'required'=>'required', 'id' => 'tipo_doc']) !!}
                                  {!! $errors->first('tipo_doc','<span class="help-block">:message</span>') !!}
                              </div>
                          </div>
                            <div class="col-md-4">
                                <label>Número de documento:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('num_doc', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'num_doc' , 'placeholder'=>'Ingrese numero de documento'  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                              <br>
                              <button type="button" class="btn btn-primary btnNext">Siguiente</button>
                            </div>
                          </div>
                          <hr/>
                          <div class="other">
                          <div class="row">
                            <div class="col-md-6">
                                <label>Apellido paterno:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'apellido_paterno' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Apellido materno:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'id' => 'apellido_materno' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Primer nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('primer_nombre', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'primer_nombre' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Segundo nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('segundo_nombre', null, ['class' => 'form-control', 'id' => 'segundo_nombre' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Fecha de nacimiento</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control form-control datepicker', 'required'=>'required', 'id' => 'fecha_nacimiento' , 'placeholder'=>''  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Género:</label>
                                <div class="input-group col-md-12 form-group {{ $errors->has('genero') ? 'has-error' :'' }}">
                                    {!! Form::select('genero', $datosGeneros, null, ['class' => 'form-control', 'required'=>'required', 'id' => 'genero']) !!}
                                    {!! $errors->first('genero','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Estado Civil:</label>
                                <div class="input-group col-md-12 form-group {{ $errors->has('estado_civil') ? 'has-error' :'' }}">
                                    {!! Form::select('estado_civil', $datosEstadosCivil, null, ['class' => 'form-control', 'required'=>'required', 'id' => 'estado_civil']) !!}
                                    {!! $errors->first('estado_civil','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                          <div class="col-md-4">
                              <label>Telefóno celular:</label>
                              <div class="input-group col-md-12 form-group">
                                  {!! Form::text('telefono_celular', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'telefono_celular' , 'placeholder'=>''  ]) !!}
                              </div>
                          </div>
                            <div class="col-md-4">
                                <label>Telefóno fijo:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('telefono_fijo', null, ['class' => 'form-control', 'id' => 'telefono_fijo' , 'placeholder'=>''  ]) !!}
                                </div>
                            </div>
                              <div class="col-md-4">
                                  <label>Correo electrónico:</label>
                                  <div class="input-group col-md-12 form-group">
                                      {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email' , 'placeholder'=>'', 'required'=>'required'  ]) !!}
                                  </div>
                              </div>
                        </div>
                        <hr/>

                        <div class="row">
                          <div class="col-md-4">
                              <label>Año de término de la educación secundaria:</label>
                              <div class="input-group col-md-12 form-group">
                                  {!! Form::text('anio_termino', null, ['class' => 'form-control form-control dateYearpicker', 'required'=>'required', 'id' => 'anio_termino' , 'placeholder'=>'' ]) !!}
                                  <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <hr/>
                        <div class="row">
                            @include('componentes.submit_reset_form_create')
                        </div>
                      </div>

                    </div>
                    {!! Form::close() !!}

                </div>
</div>

@endsection

@section('js')
  <script>
      $('.datepicker').datepicker({autoclose: true, format: 'yyyy-mm-dd',});
      $('.dateYearpicker').datepicker({
        format: 'yyyy',
        dateFormat: 'yy',
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
      });
      $(document).ready(function(){
        $('.other').hide();
      });
      $('.btnNext').click(function () {
        var tipo_doc = $('#tipo_doc').val();
        var num_doc = $('#num_doc').val();
        src = "{{ route('personas_evento.getByDoc', ['tipo_doc' => ':tipo_doc', 'num_doc' => ':num_doc']) }}";
        src = src.replace(':tipo_doc', tipo_doc);
        src = src.replace(':num_doc', num_doc);
        $.ajax({
            type: "GET",
            url: src,
            dataType: "json",
            success: function( data ) {
                $('.other').show();
                if(data != null) {
                  $('#apellido_paterno').val(data.apellido_paterno);
                  $('#apellido_materno').val(data.apellido_materno);
                  $('#primer_nombre').val(data.primer_nombre);
                  $('#segundo_nombre').val(data.segundo_nombre);
                  $('#estado_civil').val(data.estado_civil);
                  $('#genero').val(data.genero);
                  $('#telefono_fijo').val(data.telefono_fijo);
                  $('#telefono_celular').val(data.telefono_celular);
                  $('#fecha_nacimiento').val(data.fecha_nacimiento.slice(0, 10));
                  $('#email').val(data.email);
                  $('#anio_termino').val(data.anio_termino);
                  $('#id_persona').val(data.id);
                }
            }
        });
    });
  </script>
@endsection
