


<div class="portlet-body">

                <div class="panel-body">



                  <div class="table">
                      <table class="table table-bordered table-striped table-hover" id="tbl_datos_persona">
                        <tr><th>Tipo de Documento</th><td>{{ $tipo_doc }}</td></tr>
                        <tr><th>Numero de Documento</th><td>{{ $persona_evento->persona->num_doc }}</td></tr>
                        <tr><th>Nombre</th><td>{{ $persona_evento->persona->nombre_completo }}</td></tr>
                        <tr><th>Celular</th><td>{{ $persona_evento->persona->telefono_celular }}</td></tr>
                        <tr><th>Teléfono Fijo</th><td>{{ $persona_evento->persona->telefono_fijo }}</td></tr>
                        <tr><th>Correo Electrónico</th><td>{{ $persona_evento->persona->email }}</td></tr>
                        <tr><th>Estado Civil</th><td>{{ $estado_civil }}</td></tr>
                        <tr><th>Género</th><td>{{ $genero }}</td></tr>
                        <tr><th>Fecha de Nacimiento</th><td>{{ $persona_evento->persona->fecha_nacimiento->format('Y-m-d') }}</td></tr>
                        
                      </table>
                  </div>

                  <div class="table">
                      <table class="table table-bordered table-striped table-hover" id="tbl_datos_evento">
                        <tr><th>¿Confirmado?</th><th>¿Asistió?</th><th>Estado Actual</th><th>Comentario</th></tr>
                        <tr>
                          <td>{{(($persona_evento->confirmado==1) ? 'SI' : 'NO')}}</td>
                          <td>{{(($persona_evento->asistencia==1) ? 'SI' : 'NO')}}</td>
                          <td>{{ $persona_evento->persona->estado->nombre }}</td>
                          <td>{{ $persona_evento->comentario}}</td>
                        </tr>
                      </table>
                  </div>

                </div>
</div>
