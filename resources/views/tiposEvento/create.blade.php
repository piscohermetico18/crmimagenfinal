@extends('layouts.app')
@section('title')
<h3>
Crear nuevo tipo de evento
  </h3>
@endsection
@section('content')
  @if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <i class=" fa fa-check cool-green "></i>
        {{ nl2br(Session::get('success')) }}
    </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-block alert-warning">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('warning')) }}
      </div>
  @endif
  @if(Session::has('danger'))
      <div class="alert alert-block alert-danger">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('danger')) }}
      </div>
  @endif
<div class="portlet-body">
                <div class="panel-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                    <form method="POST" action="{{ url('tipos_evento') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-6">
                                <label>Nombre:</label>
                                <div class="input-group col-md-12">
                                    {!! Form::text('nombre', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'nombre' , 'placeholder'=>'Ingrese nombre del tipo de evento'  ]) !!}
                                </div>

                            </div>

                            <div class="col-md-6">
                                <label>Descripción</label>
                                <div class="input-group col-md-12">
                                    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion' , 'placeholder'=>'Ingrese descripcion', 'rows'=>'2'  ]) !!}
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            @include('componentes.submit_reset_form_create')
                        </div>

                    </div>
                    {!! Form::close() !!}

                </div>
</div>

@endsection
