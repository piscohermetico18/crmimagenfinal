@extends('layouts.app')
@section('title')
<h3>
Evento: {{ $evento->nombre }}

  </h3>
@endsection
@section('content')

<div class="portlet-body">

                <div class="panel-body">
                  <div class="table">
                      <table class="table table-bordered table-striped table-hover" id="tbl_eventos">
                        <tr><th>Tipo de Evento</th><td>{{ $evento->tipoEvento->nombre }}</td></tr>
                        <tr><th>Evento</th><td>{{ $evento->nombre }}</td></tr>
                        <tr><th>Fecha</th><td>{{ $evento->fecha_inicio->format('Y-m-d H:i') }} - {{ $evento->fecha_fin->format('Y-m-d H:i') }}</td></tr>
                        <tr><th>Lugar</th><td>{{ $evento->lugar }}</td></tr>
                        <tr><th>Institucion</th><td>{{ $evento->institucion->nombre." (".$evento->institucion->codigo_modular.")" }}</td></tr>
                        <tr><th>Descripcion</th><td>{{ $evento->lugar }}</td></tr>
                      </table>
                  </div>
                  <h3>Personas <a href="{{ route('personas_evento.create', ['id_evento' => $evento->id, 'id_institucion' => $evento->id_institucion]) }}" class="btn btn-primary pull-right btn-sm">
                          Agregar Nueva Persona
                      </a></h3>
                  <br>
                  <div class="table">
                      <table class="table table-bordered table-striped table-hover" id="tbl_personas">
                          <thead>
                              <tr>
                                  <th>NRO_DOC</th><th>Nombre</th><th>¿Confirmado?</th><th>¿Asistió?</th><th>Estado Actual</th><th>Comentario</th><th>Acciones</th>
                              </tr>
                          </thead>
                          <tbody>
                             @foreach($personasEvento as $item)
                              <tr id="tr_{{$item->id}}">
                                  <td>{{ $item->persona->num_doc }}</td>
                                  <td>{{ $item->persona->nombre_completo }}</td>
                                  <td>{{(($item->confirmado==1) ? 'SI' : 'NO')}}</td>
                                  <td>{{(($item->asistencia==1) ? 'SI' : 'NO')}}</td>
                                  <td>{{ $item->persona->estado->nombre }}</td>
                                  <td>{{ $item->comentario}}</td>
                                  <td>
                                      <a href="{{ route('personas_evento.show', ['id_evento' => $item->id_evento, 'id_persona' => $item->id_persona]) }}" class="btn btn-primary btn-xs" data-toggle="modal" data-target-id="{{ $item->id_persona }}" data-target="#myModal"><i class="fa fa-edit"></i>Detalles</a>
                                       <a href="{{ route('personas_evento.edit', ['id' => $item->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a>
                                          {!! Form::open(['method' => 'GET','route' => ['personas_evento.delete', $item->id],'style'=>'display:inline']) !!}
                                          {!! Form::button('<i class="fa fa-edit"></i>Eliminar del Evento', ['class' => 'btn btn-primary btn-xs','data-toggle'=>'confirmation',
                                            'data-btn-ok-label'=>'Eliminar del Evento', 'data-btn-ok-icon'=>'fa fa-remove',
                                            'data-btn-ok-class'=>'btn btn-sm btn-danger',
                                            'data-btn-cancel-label'=>'Cancelar',
                                            'data-btn-cancel-icon'=>'fa fa-chevron-circle-left',
                                            'data-btn-cancel-class'=>'btn btn-sm btn-default',
                                            'data-title'=>'Confirma que desea eliminar?',
                                            'data-placement'=>'left', 'data-singleton'=>'true' ]) !!}
                                          {!! Form::close() !!}
                                    </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>

                </div>
</div>

<!-- Modal -->


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="myModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detalle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
  <script type="text/javascript">
      $(document).ready(function () {
          $('#tbl_personas').DataTable({
              columnDefs: [{
                      targets: [0],
                      visible: true,
                      searchable: true
                  },
              ],
              order: [[1, "asc"]],

            "language": {

      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
              }
          }
          );
      });

        $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
              onConfirm: function (event, element) {
                  element.closest('form').submit();
              }
          });


    $(document).ready(function(){
        $("#myModal").on("show.bs.modal", function(e) {
            var ele = e.target;
            console.log(ele.href);
            $.get( ele.href, function( data ) {
                $(".modal-body").html(data.html);
            });

        });
    });

  </script>
@endsection
