@extends('layouts.app')
@section('title')
<h3>
Crear nuevo evento
  </h3>
@endsection
@section('content')
  @if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <i class=" fa fa-check cool-green "></i>
        {{ nl2br(Session::get('success')) }}
    </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-block alert-warning">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('warning')) }}
      </div>
  @endif
  @if(Session::has('danger'))
      <div class="alert alert-block alert-danger">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('danger')) }}
      </div>
  @endif
<div class="portlet-body">

                <div class="panel-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                    <form method="POST" action="{{ url('eventos') }}">
                        {{ csrf_field() }}
                        {!! Form::hidden('id_tipo_evento', 1, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_tipo_evento'  ]) !!}
                        {!! Form::hidden('id_institucion', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_institucion'  ]) !!}
                        <div class="row">

                          <div class="col-md-6">
                            <label>Tipo de Evento:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('id_tipo_evento') ? 'has-error' :'' }}">
                                {!! Form::select('id_tipo_evento', $datosTiposEvento, null, ['class' => 'form-control', 'required'=>'required']) !!}
                                {!! $errors->first('id_tipo_evento','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                          <div class="col-md-6">
                                <label>Nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('nombre', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'nombre' , 'placeholder'=>'Ingrese nombre del tipo de evento'  ]) !!}
                                </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Lugar</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('lugar', null, ['class' => 'form-control', 'id' => 'lugar' , 'placeholder'=>'Ingrese lugar'  ]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Inicio</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_inicio', null, ['class' => 'form-control form-control datetimepicker', 'required'=>'required', 'id' => 'fecha_inicio' , 'placeholder'=>'Ingrese fecha de inicio'  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Fin</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_fin', null, ['class' => 'form-control form-control datetimepicker', 'required'=>'required', 'id' => 'fecha_fin' , 'placeholder'=>'Ingrese fecha de fin'  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                                <label>Descripción</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion' , 'placeholder'=>'Ingrese descripcion', 'rows'=>'2'  ]) !!}
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                                <label>Institución:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('institucion', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'institucion' , 'placeholder'=>'Ingrese nombre de la Institucion'  ]) !!}
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            @include('componentes.submit_reset_form_create')
                        </div>
                    </div>
                    {!! Form::close() !!}
</div>

@endsection

@section('js')
  <script>
      $('.datetimepicker').datetimepicker({
        autoclose: true,
        format: 'yyyy-mm-dd hh:ii',
      });
      $(document).ready(function() {
        src = "{{ route('eventos.searchajax') }}";
         $("#institucion").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term : request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function(event, ui) {
        	  	$('#id_institucion').val(ui.item.id);
        	  }
        });
    });
  </script>
@endsection
