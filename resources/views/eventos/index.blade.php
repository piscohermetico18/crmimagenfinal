@extends('layouts.app')

@section('title')
<h1> Eventos <a href="{{ route('eventos.create') }}" class="btn btn-primary pull-right btn-sm">
        Agregar Nuevo Evento
    </a></h1>
<br>
@endsection
@section('content')

<div class="table">
    <table class="table table-bordered table-striped table-hover" id="tbl_eventos">
        <thead>
            <tr>
                <th>Tipo Evento</th><th>Nombre</th><th>Inicio</th><th>Fin</th><th>Institucion</th><th>Descripción</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
           @foreach($eventos as $item)
            <tr id="tr_{{$item->id}}">
              <td>{{ $item->tipoEvento->nombre }}</td>
                <td>{{ $item->nombre }}</td>
                <td>{{ $item->fecha_inicio->format('Y-m-d H:i') }}</td>
                <td>{{ $item->fecha_fin->format('Y-m-d H:i') }}</td>
                <td>{{ $item->institucion->nombre." (".$item->institucion->codigo_modular.")" }}</td>
                <td>{{ $item->descripcion}}</td>
                <td>
                     <a href="{{ route('eventos.show', ['id' => $item->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Detalles</a>
                      <a href="{{ route('eventos.edit', ['id' => $item->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a>
                        {!! Form::open(['method' => 'GET','route' => ['eventos.delete', $item->id],'style'=>'display:inline']) !!}
                        {!! Form::button('<i class="fa fa-edit"></i>Eliminar', ['class' => 'btn btn-primary btn-xs','data-toggle'=>'confirmation',
                          'data-btn-ok-label'=>'Eliminar', 'data-btn-ok-icon'=>'fa fa-remove',
                          'data-btn-ok-class'=>'btn btn-sm btn-danger',
                          'data-btn-cancel-label'=>'Cancelar',
                          'data-btn-cancel-icon'=>'fa fa-chevron-circle-left',
                          'data-btn-cancel-class'=>'btn btn-sm btn-default',
                          'data-title'=>'Confirma que desea eliminar?',
                          'data-placement'=>'left', 'data-singleton'=>'true' ]) !!}
                        {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>




@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_eventos').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: true
                },
            ],
            order: [[2, "desc"]],

          "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        }
        );
    });

      $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.closest('form').submit();
            }
        });
</script>
@endsection
