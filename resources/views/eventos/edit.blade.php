@extends('layouts.app')
@section('title')
<h3>
Editar evento
  </h3>
@endsection
@section('content')
  @if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <i class=" fa fa-check cool-green "></i>
        {{ nl2br(Session::get('success')) }}
    </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-block alert-warning">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('warning')) }}
      </div>
  @endif
  @if(Session::has('danger'))
      <div class="alert alert-block alert-danger">
          <i class=" fa fa-check cool-green "></i>
          {{ nl2br(Session::get('danger')) }}
      </div>
  @endif
<div class="portlet-body">
                <div class="panel-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  {!! Form::model($evento, [
          'method' => 'PUT',
          'url' => ['eventos/update'],
          'class' => 'tab-content',
          'id'=>'formulario'
          ]) !!}
                        {{ csrf_field() }}
                        {!! Form::hidden('id', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'id'  ]) !!}
                        {!! Form::hidden('id_tipo_evento', 1, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_tipo_evento'  ]) !!}
                        {!! Form::hidden('id_institucion', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_institucion'  ]) !!}
                        <div class="row">

                          <div class="col-md-6">
                            <label>Tipo de Evento:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('id_tipo_evento') ? 'has-error' :'' }}">
                                {!! Form::select('id_tipo_evento', $datosTiposEvento, $evento->id_tipo_evento, ['class' => 'form-control', 'required'=>'required']) !!}
                                {!! $errors->first('id_tipo_evento','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                            <div class="col-md-6">
                                <label>Nombre:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('nombre', $evento->nombre, ['class' => 'form-control', 'required'=>'required', 'id' => 'nombre' , 'placeholder'=>'Ingrese nombre del tipo de evento'  ]) !!}
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6">
                                <label>Lugar</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('lugar', $evento->lugar, ['class' => 'form-control', 'id' => 'lugar' , 'placeholder'=>'Ingrese lugar'  ]) !!}
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6">
                                <label>Inicio</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_inicio', $evento->fecha_inicio->format('Y-m-d H:i'), ['class' => 'form-control form-control datetimepicker', 'required'=>'required', 'id' => 'fecha_inicio' , 'placeholder'=>'Ingrese fecha de inicio'  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>Fin</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('fecha_fin', $evento->fecha_fin->format('Y-m-d H:i'), ['class' => 'form-control form-control datetimepicker', 'required'=>'required', 'id' => 'fecha_fin' , 'placeholder'=>'Ingrese fecha de fin'  ]) !!}
                                    <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6">
                                <label>Descripción</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::textarea('descripcion', $evento->descripcion, ['class' => 'form-control', 'id' => 'descripcion' , 'placeholder'=>'Ingrese descripcion', 'rows'=>'2'  ]) !!}
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <label>Institución:</label>
                                <div class="input-group col-md-12 form-group">
                                    {!! Form::text('institucion', $evento->institucion->nombre." (".$evento->institucion->codigo_modular.")", ['class' => 'form-control', 'required'=>'required', 'id' => 'institucion' , 'placeholder'=>'Ingrese nombre de la Institucion'  ]) !!}
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            @include('componentes.submit_reset_form_update')
                        </div>

                    </div>
                    {!! Form::close() !!}
</div>

@endsection

@section('js')
  <script>
      $('.datetimepicker').datetimepicker({
        autoclose: true,
        format: 'yyyy-mm-dd hh:ii',
      });
      $(document).ready(function() {
        src = "{{ route('eventos.searchajax') }}";
         $("#institucion").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term : request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function(event, ui) {
        	  	$('#id_institucion').val(ui.item.id);
        	  }
        });
    });
  </script>
@endsection
