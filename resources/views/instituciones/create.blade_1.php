@extends('layouts.app')
@section('title')
<h3>
       @if ($tipo_organizacion_id==1)
      Crear Nuevo
        @else 
        Crear Nueva 
        @endif  
        {{$tipo_organizacion_name}}
    
  </h3>
@endsection

@section('content')

<hr/>


<div class="portlet-body">
    <div class="tabbable-custom ">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_5_1" data-toggle="tab"> Organización </a>
            </li>
            <li>
                <a href="#tab_5_2" data-toggle="tab"> Junta Directiva </a>
            </li>

            @if ($tipo_organizacion_id==4)
             <li>
                <a href="#tab_5_3" data-toggle="tab"> Características Territoriales </a>
            </li>
              @else
            <li>
                <a href="#tab_5_3" data-toggle="tab"> Afiliación </a>
            </li>
            @endif
            <li>
                <a href="#tab_5_4" data-toggle="tab">Cotización </a>
            </li>


            <li>
                <a href="#tab_5_5" data-toggle="tab">Servicios</a>
            </li>


            @if ($tipo_organizacion_id==1)
            <li>
                <a href="#tab_5_6" data-toggle="tab">Empresa </a>
            </li>
            
            @endif
            @if ($tipo_organizacion_id==4)
            <li>
                <a href="#tab_5_6" data-toggle="tab">Gobierno Regional </a>
            </li>
            
            @endif
            
            
        </ul>

        {!! Form::open(['url' => 'organizaciones', 'class' => 'tab-content','id'=>'formulario']) !!}
        {!! Form::hidden('tipo_organizacion_id', $tipo_organizacion_id, ['class' => 'form-control', 'required'=>'required', 'id' => 'tipo_organizacion_id'  ]) !!}
        <div class="tab-content">

            <div  class="tab-pane active" id="tab_5_1">
                <div class="portlet-body">

  
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombre:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('nombre_organizacion', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'nombre_organizacion' , 'placeholder'=>'Ingrese nombre de organización'  ]) !!}
                            </div>

                        </div>

                        <div class="col-md-6">
                            <label>Siglas</label>
                            <div class="input-group col-md-6">
                                {!! Form::text('siglas_organizacion', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'siglas_organizacion' , 'placeholder'=>'Ingrese siglas de organización'  ]) !!}
                            </div>
                        </div>



                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Direccion Postal:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('direccion_organizacion', null, ['class' => 'form-control', 'id' => 'direccion_organizacion' , 'placeholder'=>'Ingrese dirección de organización'  ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Departamento:</label>
                            <div class="input-group col-md-12">
                                {!! $selectDepartamentos !!}
                            </div>
                        </div>
 
                        
                      @if ($tipo_organizacion_id!=4)
                       <div class="col-md-4">
                            <label>Provincia:</label>
                            <div class="input-group col-md-12">
                                <select id="provincia_id" name="provincia_id" readonly="true" class="form-control select2">
                                    <option value="0">Seleccionar</option>
                                </select>
                            </div>
                        </div>

                      @else
                            <div class="col-md-4">
                            <label>Provincia:</label>
                            <div class="input-group col-md-12">
                                <select id="ubigeo" name="ubigeo" readonly="true" class="form-control select2">
                                    <option value="0">Seleccionar</option>
                                </select>
                            </div>
                        </div>
                      @endif  
                        
                        
                        
                        @if ($tipo_organizacion_id!=4)
                       <div class="col-md-4">
                            <label>Distrito:</label>
                            <div class="input-group col-md-12">
                                
                                
                                <select id="ubigeo" name="ubigeo" readonly="true" class="form-control select2">
                                    <option value="0">Seleccionar</option>
                                </select>
                                
                                
                            </div>
                        </div>

                      @endif  
                        
                    
                        
                    </div>

                    <hr/>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Fecha Fundación:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                    {!! Form::text('fec_fundacion', null, ['class' => 'form-control', 'id' => 'fec_fundacion' ,  'placeholder'=>'dd/mm/aaaa'  ]) !!}

                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Se encuentra registrada en Registros Públicos?:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    SI
                                    {{ Form::radio('inscrito_registros_publicos', '1') }}

                                    NO {{ Form::radio('inscrito_registros_publicos', '2') }}
                                    <span></span>
                                </span>
                                {!! Form::text('num_partida_electronica', null, ['class' => 'form-control', 'id' => 'num_partida_electronica' , 'placeholder'=>'Número de partida electrónica'  ]) !!}

                            </div>

                        </div>

                        <div class="col-md-4">
                            <label>Número de registro sindical:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('num_registro_sindical', null, ['class' => 'form-control', 'id' => 'num_registro_sindical' , 'placeholder'=>'Registro sindical en el Ministerio de Trabajo'  ]) !!}

                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Número de Afiliados a la fecha:</label>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Hombres:
                                </span>
                                {!! Form::number('num_afiliados_hombres', 0, ['class' => 'form-control', 'id' => 'num_afiliados_hombres' , 'placeholder'=>'Cantidad'  ]) !!}


                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Mujeres:
                                </span>
                                {!! Form::number('num_afiliados_mujeres', 0, ['class' => 'form-control', 'id' => 'num_afiliados_mujeres' , 'placeholder'=>'Cantidad'  ]) !!}


                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Total:
                                </span>
                                {!! Form::text('num_afiliados_total', 0, ['class' => 'form-control', 'id' => 'num_afiliados_total' , 'placeholder'=>'Total', 'readonly'=>'readonly'  ]) !!}


                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Periodo de Gestión de la Junta Directiva:</label>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 136px;text-align: right;">
                                    Fecha de Inicio
                                </span>
                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">

                                    {!! Form::text('fec_inicio_junta_directiva', null, ['class' => 'form-control', 'id' => 'fec_inicio_junta_directiva' , 'readonly'=>'readonly', 'placeholder'=>'dd/mm/aaaa'  ]) !!}
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 136px;text-align: right;">
                                    Fecha de Fin
                                </span>
                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                    {!! Form::text('fec_fin_junta_directiva', null, ['class' => 'form-control', 'id' => 'fec_fin_junta_directiva' , 'readonly'=>'readonly', 'placeholder'=>'dd/mm/aaaa'  ]) !!}
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 136px;text-align: right;">
                                    Vigencia en años
                                </span>
                                {!! Form::text('vigencia_junta_directiva', null, ['class' => 'form-control', 'id' => 'vigencia_junta_directiva' , 'placeholder'=>'Número de años'  ]) !!}

                            </div>

                        </div>


                    </div>

                 @if ($tipo_organizacion_id==4)
                         <hr/>
                    <div class="row">
                       <div class="col-md-4">
                            <label>Número de sindicatos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::number('num_sindicatos', null, ['class' => 'form-control', 'id' => 'num_sindicatos' , 'placeholder'=>'Ingrese el número de sindicatos'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Número de federaciones:</label>
                            <div class="input-group col-md-12">
                                {!! Form::number('num_federaciones', null, ['class' => 'form-control', 'id' => 'num_federaciones' , 'placeholder'=>'Ingrese el número de federaciones'  ]) !!}

                            </div>
                        </div>
                         <div class="col-md-4">
                            <label>Número de provinciales:</label>
                            <div class="input-group col-md-12">
                                {!! Form::number('num_provinciales', null, ['class' => 'form-control', 'id' => 'num_provinciales' , 'placeholder'=>'Ingrese el número de provinciales'  ]) !!}

                            </div>
                        </div>
                   
                        
                    </div>
                  @endif  
                    
                 @if ($tipo_organizacion_id==2 || $tipo_organizacion_id==3 )
                         <hr/>
                    <div class="row">
                       <div class="col-md-3">
                            <label>Número de base que agrupa:</label>
                            <div class="input-group col-md-12">
                                {!! Form::number('num_bases_agrupa', null, ['class' => 'form-control', 'id' => 'num_bases_agrupa' , 'placeholder'=>'Ingrese el número de bases que agrupa'  ]) !!}

                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <label>Sector al que pertenece:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('sector_pertenencia', null, ['class' => 'form-control', 'id' => 'sector_pertenencia' , 'placeholder'=>'Ingrese el sector al que pertenece'  ]) !!}

                            </div>
                        </div>
                        
                    </div>
                  @endif  


                    <hr/>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Contacto:</label>
                            <div class="input-group col-md-12">
                                <span class="input-group-addon">
                                    Página web
                                </span>
                                {!! Form::text('pagina_web_organizacion', null, ['class' => 'form-control', 'id' => 'pagina_web_organizacion' , 'placeholder'=>'Página web'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>&nbsp;</label>
                            <div class="input-group col-md-12">
                                <span class="input-group-addon">
                                    Facebook
                                </span>
                                {!! Form::text('facebook_organizacion', null, ['class' => 'form-control', 'id' => 'facebook_organizacion' , 'placeholder'=>'Facebook'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>&nbsp;</label>
                            <div class="input-group col-md-12">
                                <span class="input-group-addon">
                                    Twitter
                                </span>
                                {!! Form::text('twitter_organizacion', null, ['class' => 'form-control', 'id' => 'twitter_organizacion' , 'placeholder'=>'Twitter'  ]) !!}

                            </div>
                        </div>

                    </div>


                </div>
            </div>

            <div  class="tab-pane" id="tab_5_2">
                <div class="portlet-body">

                    
                    @if ($tipo_organizacion_id!=1)
                    <label><strong>Presidente</strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('presidente_nombres', null, ['class' => 'form-control', 'id' => 'presidente_nombres' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>DNI:</label>
                            <div class="input-group">
                                {!! Form::text('presidente_dni', null, ['class' => 'form-control', 'id' => 'presidente_dni' , 'placeholder'=>'Ingrese DNI'  ]) !!}

                            </div>
                        </div>

                    </div>                    
					
					<div class="row">
                        <div class="col-md-3">
                            <label>Contacto:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('presidente_num_fijo', null, ['class' => 'form-control', 'id' => 'presidente_num_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('presidente_num_movil', null, ['class' => 'form-control', 'id' => 'presidente_num_movil' , 'placeholder'=>'Teléfono Móvil'  ]) !!}
                            </div>

                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('presidente_email', null, ['class' => 'form-control', 'id' => 'presidente_email' , 'placeholder'=>'Email'  ]) !!}

                            </div>

                        </div>
                    </div>
                      <hr/>
                    @endif  

                    
                    
                    
                    
                    
                    <label><strong>Secretario General</strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('secretario_general_nombres', null, ['class' => 'form-control', 'id' => 'secretario_general_nombres' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>DNI:</label>
                            <div class="input-group">
                                {!! Form::text('secretario_general_dni', null, ['class' => 'form-control', 'id' => 'secretario_general_dni' , 'placeholder'=>'Ingrese DNI'  ]) !!}

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label>Contacto:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('secretario_general_num_fijo', null, ['class' => 'form-control', 'id' => 'secretario_general_num_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}

                            </div>

                        </div>
                      <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('secretario_general_num_movil', null, ['class' => 'form-control', 'id' => 'secretario_general_num_movil' , 'placeholder'=>'Teléfono Móvil'  ]) !!}
                            </div>

                        </div>
                      
                    </div>
                    <div class="row">
                        
                       
                      <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('secretario_general_email', null, ['class' => 'form-control', 'id' => 'secretario_general_email' , 'placeholder'=>'Email'  ]) !!}

                            </div>

                        </div>
                    </div>
                


                    <hr/>
                    <label><strong>Secretario de Defensa</strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('secretario_defensa_nombres', null, ['class' => 'form-control', 'id' => 'secretario_defensa_nombres' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>DNI:</label>
                            <div class="input-group">
                                {!! Form::text('secretario_defensa_dni', null, ['class' => 'form-control', 'id' => 'secretario_defensa_dni' , 'placeholder'=>'Ingrese DNI'  ]) !!}

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Contacto:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('secretario_defensa_num_fijo', null, ['class' => 'form-control', 'id' => 'secretario_defensa_num_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('secretario_defensa_num_movil', null, ['class' => 'form-control', 'id' => 'secretario_defensa_num_movil' , 'placeholder'=>'Teléfono Móvil'  ]) !!}


                            </div>

                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('secretario_defensa_email', null, ['class' => 'form-control', 'id' => 'secretario_defensa_email' , 'placeholder'=>'Email'  ]) !!}


                            </div>

                        </div>
                    </div>

                    <hr/>
                    <label><strong>Secretario de Organización</strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('secretario_organizacion_nombres', null, ['class' => 'form-control', 'id' => 'secretario_organizacion_nombres' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}


                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>DNI:</label>
                            <div class="input-group">
                                {!! Form::text('secretario_organizacion_dni', null, ['class' => 'form-control', 'id' => 'secretario_organizacion_dni' , 'placeholder'=>'Ingrese DNI'  ]) !!}

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Contacto:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('secretario_organizacion_num_fijo', null, ['class' => 'form-control', 'id' => 'secretario_organizacion_num_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}


                            </div>

                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('secretario_organizacion_num_movil', null, ['class' => 'form-control', 'id' => 'secretario_organizacion_num_movil' , 'placeholder'=>'Teléfono Móvil'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('secretario_organizacion_email', null, ['class' => 'form-control', 'id' => 'secretario_organizacion_email' , 'placeholder'=>'Email'  ]) !!}

                            </div>

                        </div>
                    </div>

                    <hr/>
                    <label><strong>Secretario de Economía</strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('secretario_economia_nombres', null, ['class' => 'form-control', 'id' => 'secretario_economia_nombres' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}

                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>DNI:</label>
                            <div class="input-group">
                                {!! Form::text('secretario_economia_dni', null, ['class' => 'form-control', 'id' => 'secretario_economia_dni' , 'placeholder'=>'Ingrese DNI'  ]) !!}
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Contacto:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('secretario_economia_num_fijo', null, ['class' => 'form-control', 'id' => 'secretario_economia_num_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('secretario_economia_num_movil', null, ['class' => 'form-control', 'id' => 'secretario_economia_num_movil' , 'placeholder'=>'Teléfono Móvil'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('secretario_economia_email', null, ['class' => 'form-control', 'id' => 'secretario_economia_email' , 'placeholder'=>'Email'  ]) !!}


                            </div>

                        </div>
                    </div>


                </div>
            </div>

            
              @if ($tipo_organizacion_id==4)
              
                   <div  class="tab-pane " id="tab_5_3">
                <div class="portlet-body">
                    <div class="row">
                        
                        <div class="col-md-6">
                            <label>Cuantas organizaciones de la CGTP Provincial tiene en la región?:</label>
             
                             <div class="input-group">
                             
                            {!! Form::number('num_organizaciones_provincial_regional', 0, ['class' => 'form-control', 'id' => 'num_organizaciones_provincial_regional' , 'placeholder'=>'Número'  ]) !!}
                              
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                            <label>Organizaciones provinciales en la región:</label>
             
                             <div class="input-group col-md-12">
                                {!! Form::text('nombre_organizaciones_provincial_regional', null, ['class' => 'form-control', 'id' => 'nombre_organizaciones_provincial_regional' , 'placeholder'=>'Ingrese nombres de organizaciones provinciales en la regíon'  ]) !!}
                            
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
              
              @else
            
            
            <div  class="tab-pane " id="tab_5_3">
                <div class="portlet-body">
                    <div class="row">
                          @if ($tipo_organizacion_id==1)
                        <div class="col-md-6">
                            <label>A que tipo de organización de segundo grado se encuentra afiliado?:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Federación
                                    {{ Form::radio('organizacion_segundo_grado_afiliado', '2') }}

                                    Confederación Sectorial{{ Form::radio('organizacion_segundo_grado_afiliado', '3') }}
                                    <span></span>
                                </span>
                            </div>
                             <div class="input-group">
                                <span class="input-group-addon">
                            {!! Form::text('nombre_organizacion_segundo_grado_afiliado', null, ['class' => 'form-control', 'id' => 'nombre_organizacion_segundo_grado_afiliado' , 'placeholder'=>'Ingrese nombre de organización de segundo grado afiliada'  ]) !!}
                                </span>
                            </div>
                        </div>
      
                          @endif
                          
                        @if ($tipo_organizacion_id==2)
                       <div class="col-md-6">
                            <label>Es afiliado a una confederación sectorial?:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    SI
                                    {{ Form::radio('afiliado_confederacion_sectorial', '1') }}

                                    NO {{ Form::radio('afiliado_confederacion_sectorial', '2') }}
                                    <span></span>
                                </span>
                                {!! Form::text('nombre_confederacion_sectorial', null, ['class' => 'form-control', 'id' => 'nombre_confederacion_sectorial' , 'placeholder'=>'Ingrese nombre de confederación sectorial'  ]) !!}
                                {!! Form::number('anio_inicio_confederacion_sectorial', null, ['class' => 'form-control', 'id' => 'anio_inicio_confederacion_sectorial' , 'placeholder'=>'Año de ingreso a confederación sectorial'  ]) !!}

                            </div>

                        </div>
                        @endif
                        
                        
                        
                    </div>
                    <hr/>

                    <div class="row">
                        <div class="col-md-5">
                            <label>Indicar año de afiliación a la CGTP:</label>
                            {!! $select_anio_afiliacion_cgtp !!}
                        </div>
                    </div>


                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombre de la organización sindical a la que pertenece:</label>
                            <div class="input-group col-md-12">
                                 {!! $selectOrganizacionInternacionalAfiliado !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            @endif
            
            
              @if ($tipo_organizacion_id==4)
              <div  class="tab-pane " id="tab_5_4">
                <div class="portlet-body">
                    <div class="row">
                                    <div class="col-md-4">
                            <label>Respecto al aporte que cotiza la CGTP Territorial a la Nacional es:</label>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 154px;text-align: right;">
                                    Fijo(S/.)
                                    {{ Form::radio('tipo_aporte_cgtp', 'fijo') }}
                                </span>
                                {!! Form::number('aporte_cgtp_fijo', null, ['class' => 'form-control', 'id' => 'aporte_cgtp_fijo' , 'placeholder'=>'Valor'  ]) !!}



                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 154px;text-align: right;">
                                    Porcentaje(%)
                                    {{ Form::radio('tipo_aporte_cgtp', 'porcentaje') }}


                                </span>
                                {!! Form::number('aporte_cgtp_porcentaje', null, ['class' => 'form-control', 'id' => 'aporte_cgtp_porcentaje' , 'placeholder'=>'Valor'  ]) !!}


                            </div>

                            <div class="input-group col-md-3"  style="width:155px;" >
                                <span class="input-group-addon" style="text-align: right;">
                                    De acuerdo a Ley
                                    {{ Form::radio('tipo_aporte_cgtp', 'deacuerdoley') }}


                                </span>

                            </div>
                            <div class="input-group col-md-3"  style="width:155px;" >
                                <span class="input-group-addon" style="text-align: right;">
                                    Por convenio
                                    {{ Form::radio('tipo_aporte_cgtp', 'convenio') }}



                                </span>

                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Respecto al aporte que realiza la federación provincial, federaciones y sindicatos a la regional es:</label>
                            <div class="input-group">
                                <span class="input-group-addon " style="width: 155px;text-align: right;">
                                    Fijo(S/.)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'fijo') }}

                                </span>
                                {!! Form::number('aporte_fijo_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_fijo_cotiza_afiliado' , 'placeholder'=>'Valor'  ]) !!}

                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    Porcentaje(%)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'porcentaje') }}

                                </span>
                                {!! Form::number('aporte_porcentaje_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_porcentaje_cotiza_afiliado' , 'placeholder'=>'Valor'  ]) !!}


                            </div>
                             <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    De acuerdo a Ley
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'deacuerdoley') }}

                                </span>
                            </div>
                            
                             <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    Por convenio
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'convenio') }}

                                </span>
                            </div>

                        </div>
                        
                    
                        
                           
                           
                    </div>




                </div>
            </div>
@else
            <div  class="tab-pane " id="tab_5_4">
                <div class="portlet-body">
                    <div class="row">
                             @if ($tipo_organizacion_id==1)
                        <div class="col-md-4">
                            <label>Respecto al aporte que cotiza su afiliado al sindicato es:</label>
                            <div class="input-group">
                                <span class="input-group-addon " style="width: 132px;text-align: right;">
                                    Fijo(S/.)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'fijo') }}

                                </span>
                                {!! Form::number('aporte_fijo_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_fijo_cotiza_afiliado' , 'placeholder'=>'Valor'  ]) !!}

                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;">
                                    Porcentaje(%)<br>(Indicar promedio de importe)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'porcentaje') }}

                                </span>
                                {!! Form::number('aporte_porcentaje_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_porcentaje_cotiza_afiliado' , 'placeholder'=>'%', 'style'=>'height:60px;'  ]) !!}


                            </div>

                        </div>
                        
                           @endif
                           
                           
                           @if ($tipo_organizacion_id==3)
                           
                           @else
                           
                           
                        <div class="col-md-3">
                            <label>Organización a la que se encuentra cotizando:</label>
                            @if ($tipo_organizacion_id==1)
                                <div class="input-group col-md-3" style="width:200px;">
                                                 <span class="input-group-addon "  style="text-align: right;" >
                                                     Federación
                                                    {{ Form::radio('tipo_organizacion_cotizando', '2') }}

                                                 </span>

                                </div>
                             @endif
                            <div class="input-group col-md-3"  style="width:200px;" >
                                <span class="input-group-addon" style="text-align: right;">
                                    Confederación Sectorial

                                    {{ Form::radio('tipo_organizacion_cotizando', '3') }}

                                </span>

                            </div>
                            <div class="input-group col-md-3" style="width:200px;">
                                <span class="input-group-addon" style="text-align: right;" >
                                    CGTP
                                    {{ Form::radio('tipo_organizacion_cotizando', '5') }}

                                </span>

                            </div>


                        </div>
                           
                           @endif
                           
                        <div class="col-md-4">
                            <label>Respecto al aporte que realiza a la CGTP es:</label>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 154px;text-align: right;">
                                    Fijo(S/.)
                                    {{ Form::radio('tipo_aporte_cgtp', 'fijo') }}


                                </span>
                                {!! Form::number('aporte_cgtp_fijo', null, ['class' => 'form-control', 'id' => 'aporte_cgtp_fijo' , 'placeholder'=>'Valor'  ]) !!}



                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 154px;text-align: right;">
                                    Porcentaje(%)
                                    {{ Form::radio('tipo_aporte_cgtp', 'porcentaje') }}


                                </span>
                                {!! Form::number('aporte_cgtp_porcentaje', null, ['class' => 'form-control', 'id' => 'aporte_cgtp_porcentaje' , 'placeholder'=>'Valor'  ]) !!}


                            </div>
                            <label>Obligación de aporte:</label>
                            <div class="input-group col-md-3"  style="width:155px;" >
                                <span class="input-group-addon" style="text-align: right;">
                                    De acuerdo a Ley
                                    {{ Form::radio('obligacion_aporte_cgtp', 'deacuerdoley') }}


                                </span>

                            </div>
                            <div class="input-group col-md-3"  style="width:155px;" >
                                <span class="input-group-addon" style="text-align: right;">
                                    Por convenio
                                    {{ Form::radio('obligacion_aporte_cgtp', 'convenio') }}



                                </span>

                            </div>

                        </div>

                         @if ($tipo_organizacion_id==2)
                        <div class="col-md-4">
                            <label>Respecto al aporte que realiza a la confederación:</label>
                            <div class="input-group">
                                <span class="input-group-addon " style="width: 155px;text-align: right;">
                                    Fijo(S/.)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'fijo') }}

                                </span>
                                {!! Form::number('aporte_fijo_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_fijo_cotiza_afiliado' , 'placeholder'=>'Valor'  ]) !!}

                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    Porcentaje(%)
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'porcentaje') }}

                                </span>
                                {!! Form::number('aporte_porcentaje_cotiza_afiliado', null, ['class' => 'form-control', 'id' => 'aporte_porcentaje_cotiza_afiliado' , 'placeholder'=>'Valor'  ]) !!}


                            </div>
                             <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    De acuerdo a Ley
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'deacuerdoley') }}

                                </span>
                            </div>
                            
                             <div class="input-group">
                                <span class="input-group-addon" style="width: 155px;text-align: right;">
                                    Por convenio
                                    {{ Form::radio('tipo_aporte_cotiza_afiliado', 'convenio') }}

                                </span>
                            </div>

                        </div>
                        
                           @endif   
                        
                           
                           
                    </div>




                </div>
            </div>

            @endif
            
            
            
            <div  class="tab-pane" id="tab_5_5">
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-5">
                            <label>Departamento que lo atendió:</label>
                            <div class="input-group">
                                {!! $selectDepartamentosServicios !!}


                            </div>
                        </div>
                        
                        <div class="col-md-4" id='div_departamento_servicio_otros' style="display:none;">
                            <label>Nombre del departamento:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('departamento_servicio_otros', null, ['class' => 'form-control', 'id' => 'departamento_servicio_otros' , 'placeholder'=>'Ingrese nombre de otro departamento'   ]) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Motivo por el cual solicitó asesoramiento:</label>
                            <div class="input-group">
                                {!! $selectMotivos !!}
                            </div>

                        </div>
                    </div>



                </div>
            </div>

            
            
             @if ($tipo_organizacion_id==4)
              <div  class="tab-pane" id="tab_5_6">
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-6">
                            <label>Razón Social:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('razon_social_empresa', null, ['class' => 'form-control', 'id' => 'razon_social_empresa' , 'placeholder'=>'Ingrese razón social'  ]) !!}
                            </div>
                        </div>


                    </div>
                   <div class="row">
                        <div class="col-md-6">
                            <label>Nombre de gobernador regional actual:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('nombre_gobernador_regional', null, ['class' => 'form-control', 'id' => 'nombre_gobernador_regional' , 'placeholder'=>'Ingrese nombre de gobernador regional actual'  ]) !!}
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Dirección legal:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('direccion_empresa', null, ['class' => 'form-control', 'id' => 'direccion_empresa' , 'placeholder'=>'Ingrese dirección legal'  ]) !!}


                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Página Web:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('pagina_web_empresa', null, ['class' => 'form-control', 'id' => 'pagina_web_empresa' , 'placeholder'=>'Ingrese página web'  ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Número de Teléfono:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('num_telefono_gobierno_regional', null, ['class' => 'form-control', 'id' => 'num_telefono_gobierno_regional' , 'placeholder'=>'Ingrese número de teléfono'  ]) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>


      
                </div>
            </div>

             @else
            
            <div  class="tab-pane" id="tab_5_6">
                <div class="portlet-body">

                    
                    
          @if ($tipo_organizacion_id==1)
                    <div class="row">
                        <div class="col-md-6">
                            <label>Código CIIU:</label>
                            <div class="input-group">
                                {!! Form::text('codigo_ciiu', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'codigo_ciiu' , 'placeholder'=>'Ingrese Código CIIU'  ]) !!}
                            </div>
                        </div>

                    </div>
                @endif
                
                    
                    <div class="row">

                        <div class="col-md-3">
                            <label>RUC:</label>
                            <div class="input-group col-md-8 ">
                                {!! Form::text('ruc_empresa', null, ['class' => 'form-control', 'id' => 'ruc_empresa' , 'placeholder'=>'RUC Empresa'  ]) !!}


                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Razón Social:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('razon_social_empresa', null, ['class' => 'form-control', 'id' => 'razon_social_empresa' , 'placeholder'=>'Ingrese razón social'  ]) !!}
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Dirección legal:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('direccion_empresa', null, ['class' => 'form-control', 'id' => 'direccion_empresa' , 'placeholder'=>'Ingrese dirección legal'  ]) !!}


                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label>Página Web:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('pagina_web_empresa', null, ['class' => 'form-control', 'id' => 'pagina_web_empresa' , 'placeholder'=>'Ingrese página web'  ]) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>


                    <div class="row">
                        <div class="col-md-3">
                            <label>Cantidad de Trabajadores:</label>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Hombres:
                                </span>
                                {!! Form::number('num_trabajadores_empresa_hombres', 0, ['class' => 'form-control', 'id' => 'num_trabajadores_empresa_hombres' , 'placeholder'=>'Cantidad'  ]) !!}

                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Mujeres:
                                </span>
                                {!! Form::number('num_trabajadores_empresa_mujeres', 0, ['class' => 'form-control', 'id' => 'num_trabajadores_empresa_mujeres' , 'placeholder'=>'Cantidad'  ]) !!}

                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Total:
                                </span>
                                {!! Form::number('num_trabajadores_empresa_total', 0, ['class' => 'form-control', 'id' => 'num_trabajadores_empresa_total' , 'placeholder'=>'Total' ,'readonly'=>'readonly' ]) !!}

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            @endif
            
            <hr/>


            <div class="row">
                @include('componentes.submit_reset_form_create')      
            </div>

        </div>



        {!! Form::close() !!}


    </div>

</div>







@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
@endsection



@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $("#btn_enviar").click(function () {

            var url = "{{Route('organizacion.store',['tipo_organizacion_id' => $tipo_organizacion_id])}}"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "GET",
                url: url,
                data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data)
                {
                    swal(data.message, null, data.status);
                    if (data.value == 1) {
                        var finalizar = function () {
                            window.location.href = "{{Route('organizaciones',['tipo_organizacion' => $tipo_organizacion_plural])}}";
                        };
                        setTimeout(finalizar, 2000);
                    }
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });


              
             
                        
                        
                      @if ($tipo_organizacion_id!=4)
                       $('#departamento_id').change(function () {
                          cargar_provincia('departamento_id','provincia_id');
                      });


                      $('#provincia_id').change(function () {
                          cargar_distrito('provincia_id','ubigeo');
                      });

                      @else

                     $('#departamento_id').change(function () {
                          cargar_provincia('departamento_id','ubigeo');
                      });


                 
                      @endif  





    function cargar_provincia(id_change, id_contiene){

        var departamento_id = $("#"+id_change).val();

        $.post("{{ route('provincia_select')}}",
                {
                    departamento_id: departamento_id,
                    "_token": "{{ csrf_token() }}"
                }).done(function (data) {
            $('#'+id_contiene).html(data);
        }).fail(function (d) {
            alert('Error');
        });
    }

    function cargar_distrito(id_change, id_contiene){

             var provincia_id = $("#"+id_change).val();

            $.post("{{ route('distrito_select')}}",
                    {
                        provincia_id: provincia_id,
                        "_token": "{{ csrf_token() }}"
                    }).done(function (data) {
                $('#'+id_contiene).html(data);
            }).fail(function (d) {
                alert('Error');
            });
    }


    $('#num_afiliados_hombres').change(function () {
        num_afiliados_total();
    });
    $('#num_afiliados_mujeres').change(function () {
        num_afiliados_total();
    });
    function num_afiliados_total() {
        var num_afiliados_hombres = $("#num_afiliados_hombres").val();
        var num_afiliados_mujeres = $("#num_afiliados_mujeres").val();
        var num_afiliados_total = parseInt(num_afiliados_hombres) + parseInt(num_afiliados_mujeres);
        $("#num_afiliados_total").attr('value', num_afiliados_total);
    }

    $('#num_trabajadores_empresa_hombres').change(function () {
        num_trabajadores_empresa_total();
    });
    $('#num_trabajadores_empresa_mujeres').change(function () {
        num_trabajadores_empresa_total();
    });
    
    function num_trabajadores_empresa_total() {
        var num_trabajadores_empresa_hombres = $("#num_trabajadores_empresa_hombres").val();
        var num_trabajadores_empresa_mujeres = $("#num_trabajadores_empresa_mujeres").val();
        var num_trabajadores_empresa_total = parseInt(num_trabajadores_empresa_hombres) + parseInt(num_trabajadores_empresa_mujeres);
        $("#num_trabajadores_empresa_total").attr('value', num_trabajadores_empresa_total);
    }


    $('#departamento_servicio_id').change(function () {
        
        
         var departamento_servicio_id = $("#departamento_servicio_id").val();
         if (departamento_servicio_id==17){
             $("#div_departamento_servicio_otros").css('display','block');
         }else{
             //$("#departamento_servicio_otros").attr('value','');
             document.getElementById("departamento_servicio_otros").value='';
             $("#div_departamento_servicio_otros").css('display','none');
         }
         
    });


</script>


@endsection