@extends ('layouts.modal_formulario')    
<div id="div_modal" class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-dark sbold uppercase"> <i class="fa fa-plus"></i> Crear Contacto</span>
        </div>
    </div>
    <div  class="portlet-body form">

        <form role="form" id="form_sample_3" method="POST" class=""  name="form_sample_3" accept-charset="UTF-8" enctype="multipart/form-data" >
                <div id="mensajes"></div>
            <input type="hidden"  value="{{$institucion_id}}" id="sec_orden" name="institucion_id"  />


            <div class="row">
                <div class="col-md-12">
                    <label>Cargo</label>
                    <div class="input-group col-md-12">
                        <div class="input-group col-md-12 form-group {{ $errors->has('cargo_id') ? 'has-error' :'' }}">
                            {!! Form::select('cargo_id', $datosCargos,null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Nombres</label>
                    <div class="input-group col-md-12 form-group">
                        {!! Form::text('primer_nombre', null, ['class' => 'form-control', 'id' => 'primer_nombre' , 'placeholder'=>'Ingrese nombres'  ]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Apellido Paterno</label>
                    <div class="input-group col-md-12 form-group">
                        {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'id' => 'apellido_paterno' , 'placeholder'=>'Ingrese Ape. Paterno'  ]) !!}
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label>Apellido Materno</label>
                    <div class="input-group col-md-12">
                        {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'id' => 'apellido_materno' , 'placeholder'=>'Ingrese Ape. Materno'  ]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Número fijo</label>
                    <div class="input-group col-md-12 form-group">
                        {!! Form::text('telefono_fijo', null, ['class' => 'form-control', 'id' => 'telefono_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Número móvil</label>
                    <div class="input-group col-md-12 form-group">
                        {!! Form::text('telefono_celular', null, ['class' => 'form-control', 'id' => 'telefono_celular' , 'placeholder'=>'Teléfono Móvil'  ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label>Email</label>
                    <div class="input-group col-md-12 form-group">
                        {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email' , 'placeholder'=>'Email'  ]) !!}
                    </div>
                </div>
            </div>

        

            <div class="form-actions right">
                <button type="button" class="btn default" id='btn-cerrar' >Cancelar</button>
                <button type="button"  class="btn blue" onclick="btn_enviar()">Guardar</button>
            </div>


        </form>

    </div>



</div>


<script type="text/javascript">


    $(document).ready(function ()
    {
        //  $('#loading_modal').css("display", "none");
    });

    function btn_enviar() {
        var formData = new FormData(document.getElementById("form_sample_3"));
        formData.append("_token", "{{ csrf_token() }}");
        $.ajax({
            url: "{{ route('contacto.store')}}",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data.msg == 'ok') {
                $('#ajax_modal').modal('toggle');
                swal(data.message, null, data.status);
                parent.oTable.ajax.reload();
            } else {
                $("#mensajes").html(data.message);
            }
        }).error(function (data) {
        });
    }

</script>
