<?php

namespace App\Http\Controllers;

use App\Evento;
use App\PersonaEvento;
use App\Persona;
use App\Generica;
use Illuminate\Http\Request;
use App\Http\Controllers\componentes\FormulariosController;
use DB;

class PersonaEventoController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
      $this->middleware('auth');
  }



    /**
     * Display the specified resource.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function show($id_evento, $id_persona)
    {
        $persona_evento = PersonaEvento::where(['id_evento' =>  $id_evento, 'id_persona' => $id_persona])->first();
        
        $estado_civil = DB::table('generica')->where('codigo', $persona_evento->persona->estado_civil)->where('tipo', 'ESTCIV')->first()->nombre;
        $genero = DB::table('generica')->where('codigo', $persona_evento->persona->genero)->where('tipo', 'TIPGEN')->first()->nombre;
        $tipo_doc = DB::table('generica')->where('codigo', $persona_evento->persona->tipo_doc)->where('tipo', 'TIPIDE')->first()->nombre;
        return view('personas_evento.show',compact('persona_evento','id_evento', 'id_persona', 'genero','estado_civil', 'tipo_doc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_evento, $id_institucion)
    {
      $formularios = new FormulariosController();
      $estados_civil = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'ESTCIV'));
      $datosEstadosCivil = $formularios->datosSelect($estados_civil);
      $generos = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPGEN'));
      $datosGeneros = $formularios->datosSelect($generos);
      $tipos_direccion = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPDIR'));
      $datosTiposDireccion = $formularios->datosSelect($tipos_direccion);
      $tipos_doc = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPIDE'));
      $datosTiposDoc = $formularios->datosSelect($tipos_doc);
        return view('personas_evento.create', compact('datosEstadosCivil', 'datosGeneros', 'datosTiposDireccion', 'datosTiposDoc', 'id_evento', 'id_institucion'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'apellido_paterno' => 'required',
            'primer_nombre' => 'required',
            'estado_civil' => 'required',
            'genero' => 'required',
            'telefono_celular' => 'required',
            'fecha_nacimiento' => 'required',
            'tipo_doc' => 'required',
            'num_doc' => 'required',
            'email' => 'required|email',
            'id_estado' => 'required',
            'id_institucion' => 'required',
            'anio_termino' => 'required'
        ]);
        try {
          if($request->get('id_persona') == null) {
            $nombre_completo = $request->get('apellido_paterno');
            $nombre_completo .= ($request->get('apellido_materno') != null)?" ".$request->get('apellido_materno'):"";
            $nombre_completo .= " ".$request->get('primer_nombre');
            $nombre_completo .= ($request->get('segundo_nombre') != null)?" ".$request->get('segundo_nombre'):"";
            $request['nombre_completo'] = $nombre_completo;
            $persona = Persona::create($request->all());
            $request['id_persona'] = $persona->id;
          } else {
            $persona = Persona::find($request->get('id_persona'));
            $persona->apellido_paterno=$request->get('apellido_paterno');
            $persona->apellido_materno=($request->get('apellido_materno'));
            $persona->primer_nombre=$request->get('primer_nombre');
            $persona->segundo_nombre=$request->get('segundo_nombre');
            $nombre_completo = $request->get('apellido_paterno');
            $nombre_completo .= ($request->get('apellido_materno') != null)?" ".$request->get('apellido_materno'):"";
            $nombre_completo .= " ".$request->get('primer_nombre');
            $nombre_completo .= ($request->get('segundo_nombre') != null)?" ".$request->get('segundo_nombre'):"";
            $persona->nombre_completo = $nombre_completo;
            $persona->estado_civil=$request->get('estado_civil');
            $persona->genero=$request->get('genero');
            $persona->telefono_fijo=$request->get('telefono_fijo');
            $persona->telefono_celular=$request->get('telefono_celular');
            $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
            $persona->tipo_doc=$request->get('tipo_doc');
            $persona->num_doc=$request->get('num_doc');
            $persona->email=$request->get('email');
            $persona->id_estado=$request->get('id_estado');
            $persona->id_institucion=$request->get('id_institucion');
            $persona->anio_termino=$request->get('anio_termino');
            $persona->save();
            $request['id_persona'] = $persona->id;
          }
          PersonaEvento::create($request->all());
          return redirect()->route('eventos.show', ['id' => $request->get('id_evento')])
                             ->with('success','Registro creado satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
          return back()->withInput()
                          ->with('danger', $exception->getMessage());
        }
    }

    public function getByDoc($tipo_doc, $num_doc) {
        $persona=Persona::where('num_doc',$num_doc)->where('tipo_doc',$tipo_doc)->first();
        if($persona != null) {
          return $persona;
        } else {
          return new Persona;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $persona_evento = PersonaEvento::find($id);
      $formularios = new FormulariosController();
      $estados_civil = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'ESTCIV'));
      $datosEstadosCivil = $formularios->datosSelect($estados_civil);
      $generos = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPGEN'));
      $datosGeneros = $formularios->datosSelect($generos);
      $tipos_direccion = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPDIR'));
      $datosTiposDireccion = $formularios->datosSelect($tipos_direccion);
      $tipos_doc = DB::select("select codigo as id, nombre as name from generica where tipo=:tipo order by peso", array('tipo'=>'TIPIDE'));
      $datosTiposDoc = $formularios->datosSelect($tipos_doc);
      $estados = DB::select("select id, nombre as name from estado order by peso");
      $datosEstados = $formularios->datosSelect($estados);

      return view('personas_evento.edit',compact('persona_evento','id', 'datosEstadosCivil', 'datosGeneros', 'datosTiposDireccion', 'datosTiposDoc', 'datosEstados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request, [
        'apellido_paterno' => 'required',
        'primer_nombre' => 'required',
        'estado_civil' => 'required',
        'genero' => 'required',
        'telefono_celular' => 'required',
        'fecha_nacimiento' => 'required',
        'tipo_doc' => 'required',
        'num_doc' => 'required',
        'email' => 'required|email',
        'id_estado' => 'required',
        'id_institucion' => 'required',
        'anio_termino' => 'required'
      ]);
      try {
        $persona = Persona::find($request->get('id_persona'));
        $persona->apellido_paterno=$request->get('apellido_paterno');
        $persona->apellido_materno=($request->get('apellido_materno'));
        $persona->primer_nombre=$request->get('primer_nombre');
        $persona->segundo_nombre=$request->get('segundo_nombre');
        $nombre_completo = $request->get('apellido_paterno');
        $nombre_completo .= ($request->get('apellido_materno') != null)?" ".$request->get('apellido_materno'):"";
        $nombre_completo .= " ".$request->get('primer_nombre');
        $nombre_completo .= ($request->get('segundo_nombre') != null)?" ".$request->get('segundo_nombre'):"";
        $persona->nombre_completo = $nombre_completo;
        $persona->estado_civil=$request->get('estado_civil');
        $persona->genero=$request->get('genero');
        $persona->telefono_fijo=$request->get('telefono_fijo');
        $persona->telefono_celular=$request->get('telefono_celular');
        $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
        $persona->tipo_doc=$request->get('tipo_doc');
        $persona->num_doc=$request->get('num_doc');
        $persona->email=$request->get('email');
        $persona->id_estado=$request->get('id_estado');
        $persona->id_institucion=$request->get('id_institucion');
        $persona->anio_termino=$request->get('anio_termino');
        $persona->save();

        $persona_evento = PersonaEvento::find($request->id);
        $persona_evento->confirmado = $request->has('confirmado');
        $persona_evento->asistencia = $request->has('asistencia');
        $persona_evento->comentario = $request->get('comentario');
        $persona_evento->save();

        return redirect()->route('eventos.show', ['id' => $request->get('id_evento')])
                           ->with('success','Registro modificado satisfactoriamente.');
      } catch (\Illuminate\Database\QueryException $exception) {
        return back()->withInput()
                        ->with('danger', $exception->getMessage());
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $persona_evento = PersonaEvento::find($id);
      $id_evento = $persona_evento->id_evento;
      $persona_evento->delete();
      return redirect()->route('eventos.show', ['id' => $id_evento])
                         ->with('success','Registro eliminado satisfactoriamente.');
    }
}
