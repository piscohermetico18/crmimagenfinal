<?php

namespace App\Http\Controllers;

use App\Evento;
use App\TipoEvento;
use App\PersonaEvento;
use App\Institucion;
use Illuminate\Http\Request;
use App\Http\Controllers\componentes\FormulariosController;
use DB;

class EventoController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
      $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $eventos = Evento::orderByDesc('fecha_inicio')->get();
      
      
      return view('eventos.index', compact('eventos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = Evento::find($id);
        $personasEvento = PersonaEvento::where('id_evento', $id)->get();
        return view('eventos.show',compact('evento','id', 'personasEvento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tipos_evento = DB::select("select id, nombre as name from tipo_evento order by nombre");
      $formularios = new FormulariosController();
      $datosTiposEvento = $formularios->datosSelect($tipos_evento);
        return view('eventos.create', compact('datosTiposEvento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'id_tipo_evento' => 'required',
            'id_institucion' => 'required'
        ]);
        try {
          $request['fecha_inicio'] .= ':00-05';
          $request['fecha_fin'] .= ':00-05';
          Evento::create($request->all());
          return redirect()->route('eventos')
                             ->with('success','Registro creado satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
          return back()->withInput()
                          ->with('danger', $exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $evento = Evento::find($id);
      $tipos_evento = DB::select("select id, nombre as name from tipo_evento order by nombre");
      $formularios = new FormulariosController();
      $datosTiposEvento = $formularios->datosSelect($tipos_evento);
      return view('eventos.edit',compact('evento','id', 'datosTiposEvento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request, [
          'nombre' => 'required',
          'fecha_inicio' => 'required',
          'fecha_fin' => 'required',
          'id_tipo_evento' => 'required',
          'id_institucion' => 'required'
      ]);
      try {
        $request['fecha_inicio'] .= ':00-05';
        $request['fecha_fin'] .= ':00-05';
        $evento = Evento::find($request->id);
        $evento->nombre=$request->get('nombre');
        $evento->descripcion=$request->get('descripcion');
        $evento->lugar=$request->get('lugar');
        $evento->fecha_inicio=$request->get('fecha_inicio');
        $evento->fecha_fin=$request->get('fecha_fin');
        $evento->id_tipo_evento=$request->get('id_tipo_evento');
        $evento->id_institucion=$request->get('id_institucion');
        $evento->save();
        return redirect()->route('eventos')
                           ->with('success','Registro modificado satisfactoriamente.');
      } catch (\Illuminate\Database\QueryException $exception) {
        return back()->withInput()
                        ->with('danger', $exception->getMessage());
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $evento = Evento::find($id);
      $evento->delete();
      return redirect()->route('eventos')
                         ->with('success','Registro eliminado satisfactoriamente.');
    }

    public function autoComplete(Request $request) {

        $query = $request->get('term','');
        $instituciones=Institucion::where('nombre','ILIKE','%'.$query.'%')->get();
        $data=array();
        foreach ($instituciones as $item) {
                $data[]=array('value'=>$item->nombre." (".$item->codigo_modular.")",'id'=>$item->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No hay coincidencias','id'=>''];
    }

}
