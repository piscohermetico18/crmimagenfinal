<?php

namespace App\Http\Controllers\componentes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Controllers\componentes\FormulariosController;

class UbigeoController extends Controller {

    public function selectProvincia(Request $request) {

        $departamento_id = $request->departamento_id;
        $provincias  = DB::select("select distinct SUBSTRING(codubigeo, 1, 4) as id, nombre as name  from ubigeo where SUBSTRING(codubigeo, 1, 2)='" . $departamento_id . "' and SUBSTRING(codubigeo, 5, 2)='00'  and SUBSTRING(codubigeo, 3, 2)<>'00'  order by nombre");
        return response()->json($provincias);
        
    }

    public function selectDistrito(Request $request) {
        $provincia_id = $request->provincia_id;
        $distritos  = DB::select("select distinct SUBSTRING(codubigeo, 1, 6) as id, nombre as name  from ubigeo where SUBSTRING(codubigeo, 1, 4)='" . $provincia_id . "' and SUBSTRING(codubigeo, 5, 2)<>'00'   order by nombre");
        return response()->json($distritos);
    }

}
