<?php

namespace App\Http\Controllers;

use App\TipoEvento;
use Illuminate\Http\Request;

class TipoEventoController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
      $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tiposEvento = TipoEvento::orderBy('nombre')->get();
      return view('tiposEvento.index', compact('tiposEvento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposEvento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);
        try {
          TipoEvento::create($request->all());
          return redirect()->route('tipos_evento')
                             ->with('success','Registro creado satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
          return back()->withInput()
                          ->with('danger', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function show(TipoEvento $tipoEvento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $tipoEvento = TipoEvento::find($id);
      return view('tiposEvento.edit',compact('tipoEvento','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

      $this->validate($request, [
          'nombre' => 'required'
      ]);
      try {
        $tipoEvento = TipoEvento::find($request->id);
        $tipoEvento->nombre=$request->get('nombre');
        $tipoEvento->descripcion=$request->get('descripcion');
        $tipoEvento->save();
        return redirect()->route('tipos_evento')
                           ->with('success','Registro modificado satisfactoriamente.');
      } catch (\Illuminate\Database\QueryException $exception) {
         return back()->withInput()
                            ->with('danger', $exception->getMessage());
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoEvento  $tipoEvento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $tipoEvento = TipoEvento::find($id);
        $tipoEvento->delete();
        return redirect()->route('tipos_evento')
                           ->with('success','Registro eliminado satisfactoriamente.');
      } catch (\Illuminate\Database\QueryException $exception) {
        return back()->withInput()
                             ->with('danger', $exception->getMessage());
      }
    }
}
