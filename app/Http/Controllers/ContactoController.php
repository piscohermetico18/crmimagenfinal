<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Institucion;
use App\Persona;
use App\Http\Requests\InstitucionRequest;
use Yajra\Datatables\Datatables;

class ContactoController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $instituciones = DB::table('institucion')
                ->select('id', 'codigo_modular', 'nombre')
                ->orderBy('id', 'desc')
                ->get();

        //  dd($instituciones);
        return view('instituciones.index', compact('instituciones'));
    }

    public function datatableContactos(Request $request) {

        $institucion_id = $request->institucion_id;
        //$institucion_id = 1;
        $contactos = DB::table('persona')
                ->select('personal_institucion.id', 'primer_nombre', 'apellido_paterno', 'apellido_materno', 'telefono_fijo', 'telefono_celular', 'email', 'generica.nombre as cargo')
                ->join('personal_institucion', 'persona.id', '=', 'personal_institucion.persona_id')
                ->join('generica', 'generica.codigo', '=', 'personal_institucion.cargo_id')
                ->where('personal_institucion.institucion_id', $institucion_id)
                ->where('generica.tipo', 'contacto_cargo')
                ->orderBy('cargo_id', 'asc')
                ->get();

        return Datatables::of($contactos, $request)
                        ->addColumn('acciones', function ($data) {

                            $retorno = ' <a href="' . Route('contacto.edit', ['id' => $data->id]) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a> ';
                            //$retorno .= ' <a href="' . Route('contacto.edit', ['id' => $data->id]) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a> ';
                            return $retorno;
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    public function create($institucion_id) {
        $formularios = new FormulariosController();
        $cargos = DB::select("select codigo as id,  nombre as name from generica where tipo='contacto_cargo'");
        $datosCargos = $formularios->datosSelect($cargos);
        return view('contactos.create', compact('datosCargos', 'institucion_id'));
    }

    public function store(Request $request) {
        if (is_null($request->institucion_id)) {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">Contacto sin institución de asignación</div>', 'msg' => 0], 200);
        }
        if ($request->cargo_id == '0') {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">Debe seleccionar un cargo para el contacto</div>', 'msg' => 0], 200);
        }

        if (is_null($request->primer_nombre)) {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">Debe ingresar un nombre al contacto</div>', 'msg' => 0], 200);
        }

        if (is_null($request->apellido_paterno)) {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">Debe ingresar un nombre al contacto</div>', 'msg' => 0], 200);
        }

        $persona_validar_email = Persona::where([
                        ['email', '=', $request->email]
                ])->first();

        if (count($persona_validar_email) > 0) {
            return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">El email ya se encuentra registrado</div>', 'msg' => 0], 200);
        }


        $persona = Persona::where([
                        ['apellido_paterno', '=', $request->apellido_paterno],
                        ['apellido_materno', '=', $request->apellido_materno],
                        ['email', '=', $request->email]
                ])->first();


        if (count($persona) > 0) {

            $personal_institucion_validacion = DB::connection('pgsql')->table('personal_institucion')
                    ->where('institucion_id', $request->institucion_id)
                    ->where('persona_id', $persona->id)
                    ->get();

            if (count($personal_institucion_validacion) > 0) {
                return response()->json(['status' => 'warning', 'message' => '<div class="alert-danger bold">La Persona ya se encuentra registrada</div>', 'msg' => 0], 200);
            }

            DB::connection('pgsql')->beginTransaction();
            try {

                DB::connection('pgsql_sior')->table('persona')
                        ->where('apellido_paterno', $request->apellido_paterno)
                        ->where('apellido_materno', $request->apellido_materno)
                        ->where('email', $request->email)
                        ->update(['telefono_fijo' => $request->telefono_fijo], ['telefono_celular' => $request->telefono_celular]);

                DB::connection('pgsql')->table('personal_institucion')->insert(
                        [
                            'institucion_id' => $request->institucion_id,
                            'persona_id' => $persona->id,
                            'cargo_id' => $request->cargo_id
                        ]
                );
            } catch (\Illuminate\Database\QueryException $exception) {
                DB::connection('pgsql')->rollBack();
                return response()->json(['status' => 'danger', 'message' => $exception->getMessage(), 'msg' => 0], 200);
            }
            DB::connection('pgsql')->commit();

            return response()->json(['status' => 'success', 'message' => 'Contacto registrado satisfactoriamente', 'msg' => 'ok'], 200);
        }


        // $now = new \DateTime();
        //  $fechaActual = $now->format('Y-m-d H:i:s');
        DB::connection('pgsql')->beginTransaction();
        try {

            $persona_id = DB::connection('pgsql')->table('persona')->insertGetId(
                    [
                        'primer_nombre' => $request->primer_nombre,
                        'apellido_paterno' => $request->apellido_paterno,
                        'apellido_materno' => $request->apellido_materno,
                        'email' => $request->email,
                        'telefono_fijo' => $request->telefono_fijo,
                        'telefono_celular' => $request->telefono_celular,
                        'tipo_doc' => 'C',
                        'num_doc' => '1',
                        'id_estado' => 1,
                        'id_institucion' => 1,
                        'anio_termino' => '2018',
                       
                    ]
            );

            DB::connection('pgsql')->table('personal_institucion')->insert(
                    [
                        'institucion_id' => $request->institucion_id,
                        'persona_id' => $persona_id,
                        'cargo_id' => $request->cargo_id
                    ]
            );
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::connection('pgsql')->rollBack();
            return response()->json(['status' => 'danger', 'message' => $exception->getMessage(), 'msg' => 0], 200);
        }
        DB::connection('pgsql')->commit();

        return response()->json(['status' => 'success', 'message' => 'Contacto registrado satisfactoriamente', 'msg' => 'ok'], 200);
    }

    public function edit($id) {
        $institucion = Institucion::findOrFail($id);
        $formularios = new FormulariosController();
        $departamentos = DB::select("select distinct codubigeo,SUBSTRING(codubigeo, 1, 2) as id, nombre as name  from ubigeo where SUBSTRING(codubigeo, 3, 4)='0000' order by codubigeo");
        $categoriaInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria'");
        $tipoInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_tipo'");
        $categoriaOll = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria_oll'");
        $cargos = DB::select("select codigo as id,  nombre as name from generica where tipo='contacto_cargo'");
        $bachillerInter = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_bachiller_inter'");

        $datosCategoriaInstitucion = $formularios->datosSelect($categoriaInstitucion);
        $datosTipoInstitucion = $formularios->datosSelect($tipoInstitucion);
        $datosCategoriaOll = $formularios->datosSelect($categoriaOll);
        $datosCargos = $formularios->datosSelect($cargos);
        $datosBachillerInter = $formularios->datosSelect($bachillerInter);
        $datosDepartamentos = $formularios->datosSelect($departamentos);

        return view('instituciones.edit', compact('institucion', 'datosDepartamentos', 'datosCategoriaInstitucion', 'datosCategoriaOll', 'datosTipoInstitucion', 'datosBachillerInter', 'datosCargos', 'datosCategoriaInstitucion'));
    }

    public function update(InstitucionRequest $request) {
        try {

            $institucion = Institucion::findOrFail($request->id);

            if (!is_null($request->fec_aniversario)) {
                $fec_aniversario = \DateTime::createFromFormat('d/m/Y', $request->fec_aniversario);
                $fec_aniversario = $fec_aniversario->format('Y-m-d');
                $request->request->add(['fec_aniversario' => $fec_aniversario]);
            }

            $institucion->update($request->all());
            return redirect()->route('instituciones')
                            ->with('success', 'Institución actualizada satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withInput()
                            ->with('danger', $exception->getMessage());
        }
    }

    public function storeContacto(Request $request) {
        try {
            /* $persona = Persona::where([
              ['nombre_completo', '=', $request->nombre_completo]
              ])->first();
              if (count($persona) > 0) {
              return back()->withInput()
              ->with('danger', 'Error: Este nombre de persona ya se encuentra registrado');
              }
              $institucion = Institucion::where([
              ['codigo_modular', '=', $request->codigo_modular],
              ['codigo_modular', '<>', null]
              ])->first();
              if (count($institucion) > 0) {
              return back()->withInput()
              ->with('danger', 'Error: Código Modular ya existe ');
              }

              if (!is_null($request->fec_aniversario)) {
              $fec_aniversario = \DateTime::createFromFormat('d/m/Y', $request->fec_aniversario);
              $fec_aniversario = $fec_aniversario->format('Y-m-d');
              $request->request->add(['fec_aniversario' => $fec_aniversario]);
              } */

            Persona::create($request->all());
            return redirect()->route('institucion.edit', $request->id_institucion)
                            ->with('success', 'Contacto registrado satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withInput()
                            ->with('danger', $exception->getMessage());
        }
    }

}
