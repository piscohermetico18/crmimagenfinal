<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEvento extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'tipo_evento';

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function getDateFormat()
  {
    return 'Y-m-d H:i:sO';
  }

  protected $fillable = [
        'nombre', 'descripcion',
    ];

}
