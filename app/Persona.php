<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'persona';

   /**
    * The attributes that are not mass assignable.
    *
    * @var array
    */
   protected $guarded = ['id'];

   public function getDateFormat()
   {
     return 'Y-m-d H:i:sO';
   }

   public function estado()
   {
       return $this->belongsTo(Estado::class, 'id_estado');
   }

   public function Institucion()
   {
       return $this->belongsTo(Institucion::class, 'id_institucion');
   }
   protected $casts = [
        'fecha_nacimiento' => 'date',
        ];
   protected $dates = ['fecha_nacimiento', 'created_at', 'updated_at'];
   protected $fillable = ['apellido_paterno', 'apellido_materno', 'primer_nombre', 'segundo_nombre', 'nombre_completo', 'estado_civil', 'genero', 'telefono_fijo', 'telefono_celular', 'fecha_nacimiento', 'tipo_doc', 'num_doc', 'email', 'id_estado', 'id_institucion', 'anio_termino'];
}
