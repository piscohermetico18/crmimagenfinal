<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PersonaEvento extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'persona_evento';

   protected $guarded = ['id'];

   public function getDateFormat()
   {
     return 'Y-m-d H:i:sO';
   }

   public function persona()
   {
       return $this->belongsTo(Persona::class, 'id_persona');
   }

   public function evento()
   {
       return $this->belongsTo(Evento::class, 'id_evento');
   }

   protected $dates = ['created_at', 'updated_at'];
   protected $fillable = ['id_persona', 'id_evento', 'confirmado', 'asistencia', 'comentario'];


}
