<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'evento';

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function getDateFormat()
  {
    return 'Y-m-d H:i:sO';
  }

  protected $dates = ['fecha_inicio', 'fecha_fin', 'created_at', 'updated_at'];

  public function tipoEvento()
  {
      return $this->belongsTo(TipoEvento::class, 'id_tipo_evento');
  }

  public function Institucion()
  {
      return $this->belongsTo(Institucion::class, 'id_institucion');
  }

  protected $fillable = ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'lugar', 'id_tipo_evento', 'id_institucion'];

}
